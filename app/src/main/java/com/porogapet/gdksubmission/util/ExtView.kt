package com.porogapet.gdksubmission.util

import android.view.View

/**

 * Created by kakaroto on 29/09/18.
 */

fun View.visible() {
    visibility = View.VISIBLE
}

fun View.invisible() {
    visibility = View.INVISIBLE
}
