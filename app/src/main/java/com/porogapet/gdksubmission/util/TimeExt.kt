package com.porogapet.gdksubmission.util

import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

/**

 * Created by kakaroto on 29/09/18.
 */


fun String.formatDateWithDay(): String {
    var df: DateFormat = SimpleDateFormat("yyyy-MM-dd", Locale("in", "ID"))
    var date: Date? = null
    try {
        date = df.parse(this)
        df = SimpleDateFormat("EEE, dd MMM yyyy", Locale("in", "ID"))
    } catch (e: ParseException) {
        e.printStackTrace()
    }

    return df.format(date)
}