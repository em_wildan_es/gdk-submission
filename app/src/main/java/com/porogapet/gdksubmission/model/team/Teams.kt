package com.porogapet.gdksubmission.model.team

import com.google.gson.annotations.SerializedName

data class Teams(

        @field:SerializedName("teams")
        val teams: List<Team>? = null
)