package com.porogapet.gdksubmission.model.schedule

import com.google.gson.annotations.SerializedName

data class Events(

        @field:SerializedName("events")
        val events: List<Event>? = null
)