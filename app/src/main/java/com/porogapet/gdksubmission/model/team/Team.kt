package com.porogapet.gdksubmission.model.team

import com.google.gson.annotations.SerializedName

data class Team(

        @field:SerializedName("Idteam")
        val teamId: String? = null,

        @field:SerializedName("strTeam")
        val teamName: String? = null,

        @field:SerializedName("strTeamBadge")
        val teamBadge: String? = null
)