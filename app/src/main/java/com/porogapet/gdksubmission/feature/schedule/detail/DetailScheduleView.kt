package com.porogapet.gdksubmission.feature.schedule.detail

import com.porogapet.gdksubmission.base.BaseView
import com.porogapet.gdksubmission.model.team.Team

/**

 * Created by kakaroto on 28/09/18.
 */
interface DetailScheduleView : BaseView {
    fun getHomeTeam(homeTeam: Team)
    fun getAwayTeam(awayTeam: Team)
}