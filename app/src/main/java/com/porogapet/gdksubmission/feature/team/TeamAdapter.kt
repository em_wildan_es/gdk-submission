package com.porogapet.gdksubmission.feature.team

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.bumptech.glide.Glide
import com.porogapet.gdksubmission.R
import com.porogapet.gdksubmission.model.team.Team
import org.jetbrains.anko.*


/**

 * Created by kakaroto on 16/09/18.
 */
class TeamAdapter(private val teams: List<Team>)
    : RecyclerView.Adapter<TeamAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(TeamUI().createView(AnkoContext.create(parent.context, parent)))
    }

    override fun getItemCount(): Int = teams.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItem(teams[position])
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        private val ivBadge: ImageView = view.find(R.id.iv_badge)
        private val tvName: TextView = view.find(R.id.tv_name)

        fun bindItem(team: Team) {
            Glide.with(itemView.context).load(team.teamBadge).into(ivBadge)
            tvName.text = team.teamName
        }

    }

    class TeamUI : AnkoComponent<ViewGroup> {
        override fun createView(ui: AnkoContext<ViewGroup>): View {
            return with(ui) {
                linearLayout {
                    lparams(width = matchParent, height = wrapContent)
                    padding = dip(16)
                    orientation = LinearLayout.HORIZONTAL

                    imageView {
                        id = R.id.iv_badge
                    }.lparams {
                        height = dip(50)
                        width = dip(50)
                    }

                    textView {
                        id = R.id.tv_name
                        textSize = 16f
                    }.lparams {
                        margin = dip(15)
                    }
                }
            }
        }


    }
}