package com.porogapet.gdksubmission.feature.schedule.list

import android.annotation.SuppressLint
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.porogapet.gdksubmission.R
import com.porogapet.gdksubmission.model.schedule.Event
import com.porogapet.gdksubmission.util.formatDateWithDay
import kotlinx.android.synthetic.main.item_schedule.view.*

/**

 * Created by kakaroto on 27/09/18.
 */
class ScheduleAdapter(private val events: List<Event>, private val listener: (Event) -> Unit)
    : RecyclerView.Adapter<ScheduleAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_schedule, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = events.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItem(events[position], listener)
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val tvDate: TextView = view.tv_date
        private val tvScore: TextView = view.tv_score
        private val tvTeam1: TextView = view.tv_team1
        private val tvTeam2: TextView = view.tv_team2


        @SuppressLint("SetTextI18n")
        fun bindItem(event: Event, listener: (Event) -> Unit) {
            tvDate.text = event.dateEvent?.formatDateWithDay()
            val score1 = if (event.intHomeScore != null) event.intHomeScore.toString() else ""
            val score2 = if (event.intAwayScore != null) event.intAwayScore.toString() else ""
            tvScore.text = "$score1 vs $score2"

            val team1 = event.strHomeTeam.toString()
            val team2 = event.strAwayTeam.toString()

            tvTeam1.text = if (team1.length >= 10) team1.substring(0, 8) + "..." else team1
            tvTeam2.text = if (team2.length >= 10) team2.substring(0, 8) + "..." else team2

            itemView.setOnClickListener { listener(event) }
        }
    }
}