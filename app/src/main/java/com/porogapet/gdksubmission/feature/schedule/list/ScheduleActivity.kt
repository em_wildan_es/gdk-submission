package com.porogapet.gdksubmission.feature.schedule.list

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.widget.LinearLayout
import com.porogapet.gdksubmission.R
import com.porogapet.gdksubmission.feature.schedule.detail.DetailScheduleActivity
import com.porogapet.gdksubmission.model.schedule.Event
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_schedule.*
import org.jetbrains.anko.support.v4.onRefresh
import org.jetbrains.anko.toast
import javax.inject.Inject

/**

 * Created by kakaroto on 16/09/18.
 */

class ScheduleActivity : AppCompatActivity(), ScheduleView {

    private var events: MutableList<Event> = mutableListOf()
    private lateinit var adapter: ScheduleAdapter
    private var isLastPage = true

    @Inject
    lateinit var presenter: SchedulePresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_schedule)

        presenter.attachView(this)
        adapter = ScheduleAdapter(events) { matchDetail(it) }

        rvSchedule.adapter = adapter
        rvSchedule.layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL, false)

        swipeRefresh.onRefresh {
            if (isLastPage) presenter.getLastEvents() else presenter.getNextEvents()
        }

        presenter.getLastEvents()
        bottomNavigationView.setOnNavigationItemSelectedListener {
            if (it.itemId == R.id.nav_prev) {
                isLastPage = true
                presenter.getLastEvents()
                return@setOnNavigationItemSelectedListener true
            } else if (it.itemId == R.id.nav_next) {
                isLastPage = false
                presenter.getNextEvents()
                return@setOnNavigationItemSelectedListener true
            } else false
        }

    }

    override fun onDestroy() {
        presenter.detachView()
        super.onDestroy()
    }

    private fun matchDetail(event: Event) {
        val intent = Intent(this, DetailScheduleActivity::class.java)
        intent.let {
            it.putExtra("event", event)
            startActivity(it)
        }
    }

    override fun getEvents(data: List<Event>) {
        events.clear()
        events.addAll(data)
        adapter.notifyDataSetChanged()
    }

    override fun showLoading() {
        swipeRefresh.isRefreshing = true
    }

    override fun hideLoading() {
        swipeRefresh.isRefreshing = false
    }

    override fun showMessage(msg: String) {
        toast(msg)
    }
}