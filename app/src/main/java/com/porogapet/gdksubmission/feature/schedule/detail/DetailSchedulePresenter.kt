package com.porogapet.gdksubmission.feature.schedule.detail

import com.porogapet.gdksubmission.app.network.Api
import com.porogapet.gdksubmission.base.NetPresenter
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

/**

 * Created by kakaroto on 28/09/18.
 */
class DetailSchedulePresenter
@Inject constructor(compositeDisposable: CompositeDisposable, api: Api)
    : NetPresenter<DetailScheduleView>(compositeDisposable, api) {

    fun getHomeTeam(id: Int?) {
        compositeDisposable.add(api.lookUpTeam(id)
                .compose(singleSchedulers()).subscribe({
                    it.teams?.get(0)?.let { team -> view?.getHomeTeam(team) }
                }, this::processError))
    }

    fun getAwayTeam(id: Int?) {
        compositeDisposable.add(api.lookUpTeam(id)
                .compose(singleSchedulers()).subscribe({
                    it.teams?.get(0)?.let { team -> view?.getAwayTeam(team) }
                }, this::processError))
    }
}