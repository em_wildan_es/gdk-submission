package com.porogapet.gdksubmission.feature.schedule.list

import com.porogapet.gdksubmission.base.BaseView
import com.porogapet.gdksubmission.model.schedule.Event

/**

 * Created by kakaroto on 27/09/18.
 */
interface ScheduleView : BaseView {
    fun getEvents(data: List<Event>)
}