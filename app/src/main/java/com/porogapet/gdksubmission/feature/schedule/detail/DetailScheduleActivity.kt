package com.porogapet.gdksubmission.feature.schedule.detail

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.porogapet.gdksubmission.R
import com.porogapet.gdksubmission.app.GlideApp
import com.porogapet.gdksubmission.model.schedule.Event
import com.porogapet.gdksubmission.model.team.Team
import com.porogapet.gdksubmission.util.formatDateWithDay
import com.porogapet.gdksubmission.util.invisible
import com.porogapet.gdksubmission.util.visible
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_match_detail.*
import org.jetbrains.anko.toast
import timber.log.Timber
import javax.inject.Inject

/**

 * Created by kakaroto on 27/09/18.
 */
class DetailScheduleActivity : AppCompatActivity(), DetailScheduleView {

    @Inject
    lateinit var presenter: DetailSchedulePresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_match_detail)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        presenter.attachView(this)
        val event: Event = intent.getParcelableExtra("event")

        event.let {
            tvDate.text = it.dateEvent?.formatDateWithDay()
            tvScore1.text = if (it.intHomeScore != null) it.intHomeScore.toString() else ""
            tvScore2.text = if (it.intAwayScore != null) it.intAwayScore.toString() else ""
            val team1 = it.strHomeTeam.toString()
            val team2 = it.strAwayTeam.toString()
            tvTeam1.text = if (team1.length >= 12) team1.substring(0, 10) + "..." else team1
            tvTeam2.text = if (team2.length >= 12) team2.substring(0, 10) + "..." else team2
            tvFormation1.text = it.strHomeFormation
            tvFormation2.text = it.strAwayFormation
            tvGoal1.text = it.strHomeGoalDetails?.replace(";", ";\n")
            tvGoal2.text = it.strAwayGoalDetails?.replace(";", ";\n")
            tvShot1.text = if (it.intHomeShots != null) it.intHomeShots.toString() else ""
            tvShot2.text = if (it.intAwayShots != null) it.intAwayShots.toString() else ""
            tvGoalKeeper1.text = it.strHomeLineupGoalkeeper
            tvGoalKeeper2.text = it.strAwayLineupGoalkeeper
            tvDefense1.text = it.strHomeLineupDefense?.replace("; ", ";\n")
            tvDefense2.text = it.strAwayLineupDefense?.replace("; ", ";\n")
            tvMidfiled1.text = it.strHomeLineupMidfield?.replace("; ", ";\n")
            tvMidfield2.text = it.strAwayLineupMidfield?.replace("; ", ";\n")
            tvForward1.text = it.strHomeLineupForward?.replace("; ", "\n")
            tvForward2.text = it.strAwayLineupForward?.replace("; ", ";\n")
            tvSubstitutes1.text = it.strHomeLineupSubstitutes?.replace("; ", ";\n")
            tvSubstitutes2.text = it.strAwayLineupSubstitutes?.replace("; ", ";\n")
            presenter.getHomeTeam(it.idHomeTeam?.toInt())
            presenter.getAwayTeam(it.idAwayTeam?.toInt())
        }

    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }

    override fun onDestroy() {
        presenter.detachView()
        super.onDestroy()
    }

    override fun getHomeTeam(homeTeam: Team) {
        GlideApp.with(this).load(homeTeam.teamBadge).dontAnimate().into(ivTeam1)
    }

    override fun getAwayTeam(awayTeam: Team) {
        GlideApp.with(this).load(awayTeam.teamBadge).dontAnimate().into(ivTeam2)
    }

    override fun showLoading() {
        progresBaar.visible()
        constraintDetail.invisible()
    }

    override fun hideLoading() {
        progresBaar.invisible()
        constraintDetail.visible()
    }

    override fun showMessage(msg: String) {
        toast(msg)
    }
}