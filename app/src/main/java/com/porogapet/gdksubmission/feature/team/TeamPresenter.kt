package com.porogapet.gdksubmission.feature.team

import com.porogapet.gdksubmission.app.network.Api
import com.porogapet.gdksubmission.base.NetPresenter
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

/**

 * Created by kakaroto on 16/09/18.
 */
class TeamPresenter
@Inject constructor(compositeDisposable: CompositeDisposable, api: Api)
    : NetPresenter<TeamView>(compositeDisposable, api) {

    fun getTeamList(league: String?) {
        compositeDisposable.add(api.getTeams(league)
                .compose(singleSchedulers())
                .subscribe({
                    it?.teams?.let { data -> view?.getTeamList(data) }
                }, this::processError))
    }


}