package com.porogapet.gdksubmission.feature.team

import com.porogapet.gdksubmission.base.BaseView
import com.porogapet.gdksubmission.model.team.Team

/**

 * Created by kakaroto on 16/09/18.
 */
interface TeamView : BaseView {
    fun getTeamList(data: List<Team>)
}