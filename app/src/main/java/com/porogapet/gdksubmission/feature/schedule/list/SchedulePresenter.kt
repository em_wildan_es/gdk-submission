package com.porogapet.gdksubmission.feature.schedule.list

import com.porogapet.gdksubmission.app.network.Api
import com.porogapet.gdksubmission.base.NetPresenter
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

/**

 * Created by kakaroto on 27/09/18.
 */
class SchedulePresenter
@Inject constructor(compositeDisposable: CompositeDisposable, api: Api)
    : NetPresenter<ScheduleView>(compositeDisposable, api) {
    private val idEnglishPremierLeague = 4328

    fun getLastEvents() {
        compositeDisposable.add(api.getEventsPast(idEnglishPremierLeague)
                .compose(singleSchedulers())
                .subscribe({
                    it?.events?.let { data ->
                        view?.getEvents(data)
                    }
                }, this::processError))
    }

    fun getNextEvents() {
        compositeDisposable.add(api.getEventsNext(idEnglishPremierLeague)
                .compose(singleSchedulers())
                .subscribe({
                    it?.events?.let { data -> view?.getEvents(data) }
                }, this::processError))
    }
}