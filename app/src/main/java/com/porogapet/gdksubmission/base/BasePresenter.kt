package com.porogapet.gdksubmission.base

import com.porogapet.gdksubmission.app.network.Api
import io.reactivex.disposables.CompositeDisposable

/**

 * Created by kakaroto on 16/09/18.
 */
abstract class BasePresenter<T>(protected val compositeDisposable: CompositeDisposable,
                                protected var api: Api) {
    protected var view: T? = null

    fun attachView(view: T) {
        this.view = view
    }

    fun detachView() {
        compositeDisposable.dispose()
//        compositeDisposable.clear()
        view == null
    }
}