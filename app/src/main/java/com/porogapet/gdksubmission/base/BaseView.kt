package com.porogapet.gdksubmission.base

/**

 * Created by kakaroto on 16/09/18.
 */

interface BaseView {

    fun showLoading()

    fun hideLoading()

    fun showMessage(msg: String)
}