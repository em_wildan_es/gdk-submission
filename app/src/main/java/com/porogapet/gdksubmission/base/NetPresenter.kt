package com.porogapet.gdksubmission.base

import com.porogapet.gdksubmission.app.network.Api
import io.reactivex.CompletableTransformer
import io.reactivex.ObservableTransformer
import io.reactivex.SingleTransformer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.HttpException
import java.io.IOException
import java.net.SocketTimeoutException

/**

 * Created by kakaroto on 16/09/18.
 */
abstract class NetPresenter<T : BaseView>(compositeDisposable: CompositeDisposable, api: Api)
    : BasePresenter<T>(compositeDisposable, api) {

    protected fun <V> observableSchedulers(): ObservableTransformer<V, V> {
        return ObservableTransformer { upstream ->
            upstream
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnSubscribe { view?.showLoading() }
                    .doAfterTerminate { view?.hideLoading() }
        }
    }

    protected fun <V> singleSchedulers(): SingleTransformer<V, V> {
        return SingleTransformer { upstream ->
            upstream
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnSubscribe { view?.showLoading() }
                    .doAfterTerminate { view?.hideLoading() }
        }
    }

    protected fun completeableSchedulers(): CompletableTransformer {
        return CompletableTransformer { upstream ->
            upstream
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnSubscribe { view?.showLoading() }
                    .doAfterTerminate { view?.hideLoading() }
        }
    }

    protected fun processError(e: Throwable) {
        when (e) {
            is HttpException -> {
                val responseBody = e.response().errorBody()
                view?.showMessage(getErrorMessage(responseBody!!))
            }
            is SocketTimeoutException -> view?.showMessage("Connection time out.")
            is IOException -> view?.showMessage("Connection lost, please check your connection.")
            else -> e.message?.let { view?.showMessage(it) }
        }
    }

    private fun getErrorMessage(responseBody: ResponseBody): String {
        val jsonObject = JSONObject(responseBody.string())
        if (jsonObject.has("data")) return jsonObject.getString("data")
        else if (jsonObject.has("message")) return jsonObject.getString("message")
        else return ""
    }
}