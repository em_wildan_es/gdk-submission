package com.porogapet.gdksubmission.app.injection

import com.porogapet.gdksubmission.feature.schedule.detail.DetailScheduleActivity
import com.porogapet.gdksubmission.feature.schedule.list.ScheduleActivity
import com.porogapet.gdksubmission.feature.team.TeamActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by kakaroto on 16/09/18.
 */

@Module
internal abstract class BuildersModule {

    @ContributesAndroidInjector
    internal abstract fun schedluceActivity(): ScheduleActivity

    @ContributesAndroidInjector
    internal abstract fun detailScheduleActivity(): DetailScheduleActivity

    @ContributesAndroidInjector
    internal abstract fun teamActivity(): TeamActivity
}
