package com.porogapet.gdksubmission.app.network

import com.porogapet.gdksubmission.BuildConfig
import com.porogapet.gdksubmission.model.schedule.Events
import com.porogapet.gdksubmission.model.team.Team
import com.porogapet.gdksubmission.model.team.Teams
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

/**

 * Created by kakaroto on 16/09/18.
 */
interface Api {

    @GET("api/v1/json/${BuildConfig.TSDB_API_KEY}/search_all_teams.php")
    fun getTeams(@Query("l") league: String?): Single<Teams>

    @GET("api/v1/json/${BuildConfig.TSDB_API_KEY}/eventspastleague.php")
    fun getEventsPast(@Query("id") id: Int?): Single<Events>

    @GET("api/v1/json/${BuildConfig.TSDB_API_KEY}/eventsnextleague.php")
    fun getEventsNext(@Query("id") id: Int?): Single<Events>

    @GET("api/v1/json/${BuildConfig.TSDB_API_KEY}/lookupteam.php")
    fun lookUpTeam(@Query("id") id: Int?): Single<Teams>

}