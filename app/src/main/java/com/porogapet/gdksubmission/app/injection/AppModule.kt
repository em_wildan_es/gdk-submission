package com.porogapet.gdksubmission.app.injection

import android.content.Context
import com.porogapet.gdksubmission.app.App
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Singleton

/**
 * Created by kakaroto on 16/09/18.
 */

@Module
class AppModule {

    @Provides
    @Singleton
    fun provideContext(app: App): Context = app.applicationContext

    @Provides
    fun provideCompositeDisposable(): CompositeDisposable = CompositeDisposable()
}
