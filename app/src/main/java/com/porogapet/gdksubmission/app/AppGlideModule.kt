package com.porogapet.gdksubmission.app

import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule



/**
 * Created by didik on 19/11/17.
 */
@GlideModule
class AppGlideModule : AppGlideModule()